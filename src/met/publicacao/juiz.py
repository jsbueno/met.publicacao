# encoding: utf-8

from five import grok
from zope import schema

from plone.directives import form, dexterity
from plone.app.textfield import RichText
from plone.namedfile.field import NamedImage

from met.publicacao import publicacaoMessageFactory as _

class IJuiz(form.Schema):
    title = schema.TextLine(
            title=_(u"Nome"),
            description= _(u"Nome do juiz"),
        )
    
    description = schema.Text(
            title=_(u"Currículo"),
        )
    
    nascimento = schema.Date(title=_(u"Data de nascimento"), required=False)

    foto = NamedImage(title=_(u"Foto"), required=False)