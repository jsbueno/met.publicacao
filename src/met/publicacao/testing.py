from plone.app.testing import PloneWithPackageLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

import met.publicacao


MET_PUBLICACAO = PloneWithPackageLayer(
    zcml_package=met.publicacao,
    zcml_filename='testing.zcml',
    gs_profile_id='met.publicacao:testing',
    name="MET_PUBLICACAO")

MET_PUBLICACAO_INTEGRATION = IntegrationTesting(
    bases=(MET_PUBLICACAO, ),
    name="MET_PUBLICACAO_INTEGRATION")

MET_PUBLICACAO_FUNCTIONAL = FunctionalTesting(
    bases=(MET_PUBLICACAO, ),
    name="MET_PUBLICACAO_FUNCTIONAL")
