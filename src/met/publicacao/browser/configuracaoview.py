# coding: utf-8

from zope.interface import implements, Interface

from Products.Five import BrowserView
from Products.CMFCore.utils import getToolByName

from met.publicacao import publicacaoMessageFactory as _
import pickle

import json

class IconfiguracaoView(Interface):
    """
    configuracao view interface
    """

    def test():
        """ test method"""


class configuracaoView(BrowserView):
    """
    configuracao browser view
    """
    implements(IconfiguracaoView)

    def __init__(self, context, request):
        self.context = context
        self.request = request
        if "nome" in self.request.form:
            self.atualiza_dados()

    def atualiza_dados(self):
        dados = self.dados_cadastro()
        dados.append(
            {"nome": self.request.form["nome"].decode("utf-8"),
             "endereco": self.request.form.get("endereço", "").decode("utf-8")        
            } )
        folder = __file__.rsplit("/",1)[0]
        arq = open(folder + "/dados.dat", "wb")
        pickle.dump(dados, arq)
        arq.close()
    
    def dados_cadastro(self):
        folder = __file__.rsplit("/",1)[0]
        try:
            arq = open(folder + "/dados.dat", "rb")
            dados = pickle.load(arq)
            arq.close()
        except Exception:
            dados = []
        return dados
    
    @property
    def portal_catalog(self):
        return getToolByName(self.context, 'portal_catalog')

    @property
    def portal(self):
        return getToolByName(self.context, 'portal_url').getPortalObject()

    def test(self):
        """
        test method
        """
        dummy = _(u'a dummy string')

        return {'dummy': dummy}
    """    
    def __call__(self):
        a = {"fruta": "banana",
             "livro": 
                {"titulo": "Nome da Rosa", 
                  "autor": "Umberto Eco"
                },
              "numeros": range(40)
            }
     """   
                 
