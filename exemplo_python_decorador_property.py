class Pessoa(object):
    def __init__(self, nome="", idade=None):
        self.nome = nome
        self.idade = idade
    
    def _set_nome(self, nome):
        n1 = nome.replace(" ", "")
        if  n1!="" and not n1.isalpha() :
            raise ValueError("Nome só pode conter letras e espacos")
        self.__nome = nome
    def _get_nome(self):
        return self.__nome
    
    nome = property(_get_nome, _set_nome)


    
def logger(func):
    def substituta(*args, **kw):
        print args, kw
        return func(*args, **kw)
    return substituta

    
#uso do decorador:
@logger
def soma(a, b):
    return a + b
    
# (o decorador é o mesmo que: )
def soma(a, b):
    return a + b

soma = logger(soma)